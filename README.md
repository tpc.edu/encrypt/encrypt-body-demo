## 介绍
本文参考[源码](https://github.com/Licoy/encrypt-body-spring-boot-starter), `encrypt-body-demo`是对SpringBoot控制器统一的响应体加密与请求体解密的注解处理方式，支持AES/DES/RSA/Base64。

## 加密解密支持
- 可进行加密的方式有：
    - - [x] AES
    - - [x] DES
    - - [ ] RSA
    - - [x] Base64
- 可进行解密的方式有：
    - - [x] AES
    - - [x] DES
    - - [ ] RSA
    - - [x] Base64
## 使用方法

- 在工程对应的`Application`类中增加@EnableEncryptBody注解，例如：
```java
@EnableEncryptBody
@SpringBootApplication
public class Application {
    
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
```
- 参数配置
在项目的`application.yml`或`application.properties`文件中进行参数配置，例如：
```yaml
encrypt:  
    body:
      aes-key: 12345678 #AES加密秘钥
      des-key: 12345678 #DES加密秘钥
```
- 对控制器响应体进行加密
```java
@Controller
@RequestMapping("/test")
public class TestController {

    @GetMapping
    @ResponseBody
    @EncryptBody(value = EncryptBodyMethod.AES)
    public String test(){
        return "hello world";
    }

}
```
或者使用`@RestController`对整个控制器的方法响应体都进行加密：
```java
@RestController
@EncryptBody
@RequestMapping("/test")
public class TestController {

    @GetMapping
    public String test(){
        return "hello world";
    }

}
```
## 注解一览表
- [加密注解一览表](https://github.com/Licoy/encrypt-body-spring-boot-starter/wiki/加密注解一览表)
- [解密注解一览表](https://github.com/Licoy/encrypt-body-spring-boot-starter/wiki/解密注解一览表)
## 开源协议
[Apache 2.0](/LICENSE)

## 其他参考
- web端可以使用[crypto-js](https://github.com/brix/crypto-js)
- [本文源码](https://gitlab.com/tpc.edu/encrypt/encrypt-body-demo)  
- [源码-参考](https://github.com/Licoy/encrypt-body-spring-boot-starter)
- [源码-参考](https://gitee.com/licoy/encrypt-body-spring-boot-starter)
- [源码-hermesdi/api-data-security](https://gitee.com/hermesdi/api-data-security/tree/master/api-security/src/main/java/hermesdi/api/security/annotation)  
- [博客-SpringBoot接口加密解密统一处理](http://www.3qphp.com/java/framework/3394.html)  
- [源码-xxssyyyyssxx/affect-inoutput](https://gitee.com/xxssyyyyssxx/affect-inoutput/tree/master/affect-inoutput-crypto/src/main/java/top/jfunc/affect/crypto)

## 实现思路
1. 设置启动加解密API请求或响应
2. 编写`RequestBodyAdvice`及`ResponseBodyAdvice`实现
3. supports引入是否加解密
4. 实现`beforeBodyRead` `beforeBodyWrite`
5. 不同方式实现加解密(动态key)