package edu.tpc.encryptbody.advice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.tpc.encryptbody.annotation.encrypt.*;
import edu.tpc.encryptbody.annotation.exclude.ExcludeEncryptBody;
import edu.tpc.encryptbody.bean.EncryptAnnotationInfoBean;
import edu.tpc.encryptbody.config.EncryptBodyConfig;
import edu.tpc.encryptbody.enums.EncryptBodyMethod;
import edu.tpc.encryptbody.exception.EncryptBodyFailException;
import edu.tpc.encryptbody.exception.EncryptMethodNotFoundException;
import edu.tpc.encryptbody.util.AESEncryptUtil;
import edu.tpc.encryptbody.util.Base64EncryptUtil;
import edu.tpc.encryptbody.util.CheckUtils;
import edu.tpc.encryptbody.util.DESEncryptUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.lang.annotation.Annotation;

/**
 * 响应数据的加密处理<br>
 *     本类只对控制器参数中含有<strong>{@link org.springframework.web.bind.annotation.ResponseBody}</strong>
 *     或者控制类上含有<strong>{@link org.springframework.web.bind.annotation.RestController}</strong>
 *     以及package为<strong><code>edu.tpc.encryptbody.annotation.encrypt</code></strong>下的注解有效
 * @see ResponseBodyAdvice
 * @author licoy.cn
 * @version 2018/9/4
 */
@Order(1)
@ControllerAdvice
@Slf4j
public class EncryptResponseBodyAdvice implements ResponseBodyAdvice {

    private final ObjectMapper objectMapper;

    private final EncryptBodyConfig config;

    @Autowired
    public EncryptResponseBodyAdvice(ObjectMapper objectMapper, EncryptBodyConfig config) {
        this.objectMapper = objectMapper;
        this.config = config;
    }


    @Override
    public boolean supports(MethodParameter methodParameter, Class converterType) {
        if (!config.getClient().contains("")){
            return false;
        }
        // 方法上有排除加密注解
        if (methodParameter.hasMethodAnnotation(ExcludeEncryptBody.class)) {
            return false;
        }
        // 方法上注解
        Annotation[] methodAnnotations = methodParameter.getMethodAnnotations();
        for (Annotation annotation : methodAnnotations) {
            if (annotation instanceof EncryptBody ||
                    annotation instanceof AESEncryptBody ||
                    annotation instanceof DESEncryptBody ||
                    annotation instanceof RSAEncryptBody ||
                    annotation instanceof Base64EncryptBody) {
                return true;
            }
        }
        // 类上注解
        Annotation[] classAnnotations = methodParameter.getDeclaringClass().getAnnotations();
        for (Annotation annotation : classAnnotations) {
            if (annotation instanceof EncryptBody ||
                    annotation instanceof AESEncryptBody ||
                    annotation instanceof DESEncryptBody ||
                    annotation instanceof RSAEncryptBody ||
                    annotation instanceof Base64EncryptBody) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter methodParameter, MediaType selectedContentType,
                                  Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        if(body==null){
            return null;
        }
        response.getHeaders().setContentType(MediaType.TEXT_PLAIN);
        String str = null;
        try {
            str = objectMapper.writeValueAsString(body);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        EncryptAnnotationInfoBean methodAnnotation = getMethodAnnotation(methodParameter);
        if(methodAnnotation!=null){
            return switchEncrypt(str, methodAnnotation);
        }
        EncryptAnnotationInfoBean classAnnotation = getClassAnnotation(methodParameter);
        if(classAnnotation!=null){
            return switchEncrypt(str, classAnnotation);
        }
        throw new EncryptBodyFailException("Encryption error, " +
                "please check if the selected source data is encrypted correctly." +
                " (加密错误，请检查选择的源数据的加密方式是否正确。)");
    }

    /**
     * 获取方法控制器上的加密注解信息
     * @param methodParameter 控制器方法
     * @return 加密注解信息
     */
    private EncryptAnnotationInfoBean getMethodAnnotation(MethodParameter methodParameter){
        Annotation[] methodAnnotations = methodParameter.getMethodAnnotations();
        return getAnnotation(methodAnnotations);
    }

    /**
     * 获取类控制器上的加密注解信息
     * @param methodParameter 控制器方法
     * @return 加密注解信息
     */
    private EncryptAnnotationInfoBean getClassAnnotation(MethodParameter methodParameter){
        Annotation[] classAnnotations = methodParameter.getDeclaringClass().getDeclaredAnnotations();
        return getAnnotation(classAnnotations);
    }

    /**
     * 获取加密注解信息
     * @param annotations 注解数组
     * @return 加密注解信息
     */
    private EncryptAnnotationInfoBean getAnnotation(Annotation[] annotations){
        if(annotations!=null && annotations.length>0){
            for (Annotation annotation : annotations) {
                if(annotation instanceof EncryptBody){
                    EncryptBody encryptBody = (EncryptBody) annotation;
                    return EncryptAnnotationInfoBean.builder()
                            .encryptBodyMethod(encryptBody.value())
                            .key(encryptBody.otherKey())
                            .build();
                }
                if(annotation instanceof Base64EncryptBody){
                    return EncryptAnnotationInfoBean.builder()
                            .encryptBodyMethod(EncryptBodyMethod.Base64)
                            .build();
                }
                if(annotation instanceof DESEncryptBody){
                    return EncryptAnnotationInfoBean.builder()
                            .encryptBodyMethod(EncryptBodyMethod.DES)
                            .key(((DESEncryptBody) annotation).otherKey())
                            .build();
                }
                if(annotation instanceof AESEncryptBody){
                    return EncryptAnnotationInfoBean.builder()
                            .encryptBodyMethod(EncryptBodyMethod.AES)
                            .key(((AESEncryptBody) annotation).otherKey())
                            .build();
                }
            }
        }
        return null;
    }

    /**
     * 选择加密方式并进行加密
     * @param formatStringBody 目标加密字符串
     * @param infoBean 加密信息
     * @return 加密结果
     */
    private String switchEncrypt(String formatStringBody,EncryptAnnotationInfoBean infoBean){
        EncryptBodyMethod method = infoBean.getEncryptBodyMethod();
        if(method==null){
            throw new EncryptMethodNotFoundException();
        }
        if(method == EncryptBodyMethod.Base64){
            return Base64EncryptUtil.encrypt(formatStringBody);
        }
        String key = infoBean.getKey();
        if(method == EncryptBodyMethod.DES){
            key = CheckUtils.checkAndGetKey(config.getDesKey(),key,"DES-KEY");
            return DESEncryptUtil.encrypt(formatStringBody,key);
        }
        if(method == EncryptBodyMethod.AES){
            key = CheckUtils.checkAndGetKey(config.getAesKey(),key,"AES-KEY");
            return AESEncryptUtil.encrypt(formatStringBody,key);
        }
        throw new EncryptBodyFailException();
    }


}
