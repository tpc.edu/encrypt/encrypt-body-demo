package edu.tpc.encryptbody.advice;

import edu.tpc.encryptbody.annotation.decrypt.*;
import edu.tpc.encryptbody.annotation.exclude.ExcludeDecryptBody;
import edu.tpc.encryptbody.bean.DecryptAnnotationInfoBean;
import edu.tpc.encryptbody.bean.DecryptHttpInputMessage;
import edu.tpc.encryptbody.config.EncryptBodyConfig;
import edu.tpc.encryptbody.enums.DecryptBodyMethod;
import edu.tpc.encryptbody.exception.DecryptBodyFailException;
import edu.tpc.encryptbody.exception.DecryptMethodNotFoundException;
import edu.tpc.encryptbody.util.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * 请求数据的加密信息解密处理<br>
 *     本类只对控制器参数中含有<strong>{@link org.springframework.web.bind.annotation.RequestBody}</strong>
 *     以及package为<strong><code>edu.tpc.encryptbody.annotation.decrypt</code></strong>下的注解有效
 * @see RequestBodyAdvice
 * @author licoy.cn
 * @version 2018/9/7
 */
@Order(1)
@ControllerAdvice
@Slf4j
public class DecryptRequestBodyAdvice implements RequestBodyAdvice {

    @Autowired
    private EncryptBodyConfig config;

    @Override
    public boolean supports(MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        if (!config.getClient().contains("")){
            return false;
        }
        // 方法上有排除解密注解
        if (methodParameter.hasMethodAnnotation(ExcludeDecryptBody.class)) {
            return false;
        }
        // 方法上注解
        Annotation[] methodAnnotations = methodParameter.getMethodAnnotations();
        for (Annotation annotation : methodAnnotations) {
            if (annotation instanceof DecryptBody ||
                    annotation instanceof AESDecryptBody ||
                    annotation instanceof DESDecryptBody ||
                    annotation instanceof RSADecryptBody ||
                    annotation instanceof Base64DecryptBody) {
                return true;
            }
        }
        // 类上注解
        Annotation[] classAnnotations = methodParameter.getDeclaringClass().getAnnotations();
        for (Annotation annotation : classAnnotations) {
            if (annotation instanceof DecryptBody ||
                    annotation instanceof AESDecryptBody ||
                    annotation instanceof DESDecryptBody ||
                    annotation instanceof RSADecryptBody ||
                    annotation instanceof Base64DecryptBody) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Object handleEmptyBody(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return body;
    }

    @Override
    public HttpInputMessage beforeBodyRead(HttpInputMessage inputMessage, MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) throws IOException {
        if(inputMessage.getBody()==null){
            return inputMessage;
        }
        String body;
        try {
            body = IOUtils.toString(inputMessage.getBody(),config.getEncoding());
        }catch (Exception e){
            throw new DecryptBodyFailException("Unable to get request body data," +
                    " please check if the sending data body or request method is in compliance with the specification." +
                    " (无法获取请求正文数据，请检查发送数据体或请求方法是否符合规范。)");
        }
        if(body==null || StringUtils.isNullOrEmpty(body)){
            throw new DecryptBodyFailException("The request body is NULL or an empty string, so the decryption failed." +
                    " (请求正文为NULL或为空字符串，因此解密失败。)");
        }
        String decryptBody = null;
        DecryptAnnotationInfoBean methodAnnotation = this.getMethodAnnotation(methodParameter);
        if(methodAnnotation!=null){
            decryptBody = switchDecrypt(body,methodAnnotation);
        }else{
            DecryptAnnotationInfoBean classAnnotation = this.getClassAnnotation(methodParameter);
            if(classAnnotation!=null){
                decryptBody = switchDecrypt(body,classAnnotation);
            }
        }
        if(decryptBody==null){
            throw new DecryptBodyFailException("Decryption error, " +
                    "please check if the selected source data is decrypted correctly." +
                    " (解密错误，请检查选择的源数据的解密方式是否正确。)");
        }
        try {
            InputStream inputStream = IOUtils.toInputStream(decryptBody, config.getEncoding());
            return new DecryptHttpInputMessage(inputStream,inputMessage.getHeaders());
        }catch (Exception e){
            throw new DecryptBodyFailException("The string is converted to a stream format exception." +
                    " Please check if the format such as encoding is correct." +
                    " (字符串转换成流格式异常，请检查编码等格式是否正确。)");
        }
    }

    @Override
    public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return body;
    }

    /**
     * 获取方法控制器上的解密注解信息
     * @param methodParameter 控制器方法
     * @return 解密注解信息
     */
    private DecryptAnnotationInfoBean getMethodAnnotation(MethodParameter methodParameter){
        Annotation[] methodAnnotations = methodParameter.getMethodAnnotations();
        return getAnnotation(methodAnnotations);
    }

    /**
     * 获取类控制器上的解密注解信息
     * @param methodParameter 控制器类
     * @return 解密注解信息
     */
    private DecryptAnnotationInfoBean getClassAnnotation(MethodParameter methodParameter){
        Annotation[] classAnnotations = methodParameter.getDeclaringClass().getDeclaredAnnotations();
        return getAnnotation(classAnnotations);
    }

    /**
     * 获取解密注解信息
     * @param annotations 控制器类
     * @return 解密注解信息
     */
    private DecryptAnnotationInfoBean getAnnotation(Annotation[] annotations){
        if(annotations!=null && annotations.length>0){
            for (Annotation annotation : annotations) {
                if(annotation instanceof DecryptBody){
                    DecryptBody decryptBody = (DecryptBody) annotation;
                    return DecryptAnnotationInfoBean.builder()
                            .decryptBodyMethod(decryptBody.value())
                            .key(decryptBody.otherKey())
                            .build();
                }
                if(annotation instanceof Base64DecryptBody){
                    return DecryptAnnotationInfoBean.builder()
                            .decryptBodyMethod(DecryptBodyMethod.Base64)
                            .build();
                }
                if(annotation instanceof DESDecryptBody){
                    return DecryptAnnotationInfoBean.builder()
                            .decryptBodyMethod(DecryptBodyMethod.DES)
                            .key(((DESDecryptBody) annotation).otherKey())
                            .build();
                }
                if(annotation instanceof AESDecryptBody){
                    return DecryptAnnotationInfoBean.builder()
                            .decryptBodyMethod(DecryptBodyMethod.AES)
                            .key(((AESDecryptBody) annotation).otherKey())
                            .build();
                }
            }
        }
        return null;
    }

    /**
     * 选择加密方式并进行解密
     * @param formatStringBody 目标解密字符串
     * @param infoBean 加密信息
     * @return 解密结果
     */
    private String switchDecrypt(String formatStringBody,DecryptAnnotationInfoBean infoBean){
        DecryptBodyMethod method = infoBean.getDecryptBodyMethod();
        if(method==null){
            throw new DecryptMethodNotFoundException();
        }
        if(method == DecryptBodyMethod.Base64){
            return Base64EncryptUtil.decrypt(formatStringBody);
        }
        String key = infoBean.getKey();
        if(method == DecryptBodyMethod.DES){
            key = CheckUtils.checkAndGetKey(config.getDesKey(),key,"DES-KEY");
            return DESEncryptUtil.decrypt(formatStringBody,key);
        }
        if(method == DecryptBodyMethod.AES){
            key = CheckUtils.checkAndGetKey(config.getAesKey(),key,"AES-KEY");
            return AESEncryptUtil.decrypt(formatStringBody,key);
        }
        throw new DecryptBodyFailException();
    }
}
