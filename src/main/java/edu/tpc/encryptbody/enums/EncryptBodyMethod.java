package edu.tpc.encryptbody.enums;

/**
 * <p>加密方式</p>
 * @author licoy.cn
 * @version 2018/9/4
 */
public enum EncryptBodyMethod {

    DES,AES,RSA,Base64

}
