package edu.tpc.encryptbody.annotation.exclude;

import java.lang.annotation.*;

/**
 * <p>排除解密含有{@link org.springframework.web.bind.annotation.RequestBody}注解的参数请求数据，可用于某个控制器或者注解上</p>
 * @author licoy.cn
 * @version 2018/9/7
 */
@Target(value = {ElementType.METHOD,ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ExcludeDecryptBody {
}
