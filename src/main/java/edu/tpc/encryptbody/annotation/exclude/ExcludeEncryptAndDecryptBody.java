package edu.tpc.encryptbody.annotation.exclude;

import java.lang.annotation.*;

/**
 * <p>排除加密{@link org.springframework.web.bind.annotation.ResponseBody}响应数据，可用于某个控制器上</p>
 * <p>排除解密含有{@link org.springframework.web.bind.annotation.RequestBody}注解的参数请求数据，可用于某个控制器上</p>
 * @author licoy.cn
 * @version 2018/9/4
 */
@Target(value = {ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@ExcludeEncryptBody
@ExcludeDecryptBody
public @interface ExcludeEncryptAndDecryptBody {
}
