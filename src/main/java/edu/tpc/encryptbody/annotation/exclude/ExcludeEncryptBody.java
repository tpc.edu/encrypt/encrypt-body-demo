package edu.tpc.encryptbody.annotation.exclude;

import java.lang.annotation.*;

/**
 * <p>排除加密{@link org.springframework.web.bind.annotation.ResponseBody}响应数据，可用于某个控制器或者注解上</p>
 * @author licoy.cn
 * @version 2018/9/4
 */
@Target(value = {ElementType.METHOD,ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ExcludeEncryptBody {
}
